import React from "react";
import ReactDOM from "react-dom/client";
import { BrowserRouter,Route ,Switch,} from "react-router-dom";
import "./index.css";
import App from "./App";
import Cart from "./component/Cart";
import reportWebVitals from "./reportWebVitals";

import Popup from "./component/Popup";

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
    <BrowserRouter>
    <Switch>
    <Route exact path="/" component={App}/>
    <Route path="/cart" component={Cart}/>
    <Route path="/products/:id" component={Popup}/>
    </Switch>
      
    </BrowserRouter>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
