import React, { Component } from "react";
import axios from "axios";
import { Link } from "react-router-dom";
import Detail from "./component/Detail";
import Header from "./component/Header";
import Loader from "./component/Loader";

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      products: [],
      failedAPI: false,
      fetchedData: false,
    };
  }

  componentDidMount() {
    this.fetchData();
  }
  fetchData = () => {
    axios.get("https://fakestoreapi.com/products")
      .then((response) => {
        this.setState({
          products: response.data,
          fetchedData: true,
        });
      })
      .catch((err) => {
        this.setState = {
          fetchData: true,
          failedAPI: true,
        };
      });
  };

  render() {
    return (
      <div>
        <Header />
        <div className="row row-cols-1 row-cols-md-3 g-4">
          {this.state.fetchedData ? (
            this.state.products.map((product) => {
              return (
                <div key={product.id}>
                  <Link to={`/products/${product.id}`}>
                    <Detail
                      imgLink={product.image}
                      title={product.title}
                      price={product.price}
                      rate={product.rating.rate}
                      count={product.rating.count}
                    />
                  </Link>
                </div>
              );
            })
          ) : this.state.failedAPI ? (
            "Sorry data not fetch"
          ) : (
            <Loader className="loader" />
          )}
        </div>
      </div>
    );
  }
}
