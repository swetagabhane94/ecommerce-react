import React, { Component } from "react";
import axios from "axios";
import Loader from "./Loader";
import { Link } from "react-router-dom";
import CartData from "./CartData";

export default class Cart extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      failedAPI: false,
      fetchedData: false,
    };
  }

  componentDidMount() {
    this.fetchData();
  }

  fetchData = () => {
    axios.get("https://fakestoreapi.com/carts")
      .then((response) => {
        this.setState({
          data: response.data,
          fetchedData: true,
        });
      })
      .catch((err) => {
        this.setState = {
          fetchData: true,
          failedAPI: true,
        };
      });
  };

  render() {
    return (
      <div>
        <header>
          <nav className="navbar navbar-light bg-primary mb-4  d-flex flex-row justify-content-evenly">
            <h1 className="navbar-brand text-light">Cart Detail</h1>
            <Link to={`/`}>
              <button className="btn btn-outline-light text-light ">
                Home
              </button>
            </Link>
          </nav>
        </header>
        {this.state.fetchedData ? (
        <div>
          {this.state.data.length === 0 ? (
            <Loader />
          ) : (
            this.state.data.map((item) => {
              return (
                <div key={`${item.id}${item.userId}`}>
               
                  <h3 className="text-primary p-2 m-2 text-center">For UserId :- {item.userId}</h3>
                  <CartData product={item.products} />
                </div>
               
              );
            })
          )}
        </div>
        ) : this.state.failedAPI ? (
          "Sorry data not fetch"
        ) : (
          <Loader className="loader" />
        )}

      </div>
    );
  }
}
