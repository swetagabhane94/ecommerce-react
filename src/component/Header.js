import React, { Component } from "react";
import { Link } from "react-router-dom";
export default class Header extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <div>
        <nav className="navbar navbar-light bg-primary mb-4  d-flex flex-row justify-content-evenly">
          <h1 className="navbar-brand text-light">
            Welcome to E-commerce project
          </h1>
          <Link to={`/cart`}>
            <button className="btn btn-outline-light text-light">Cart</button>
          </Link>
        </nav>
      </div>
    );
  }
}
