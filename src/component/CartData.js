import React, { Component } from "react";
import axios from "axios";
import Loader from "./Loader";

export default class CartData extends Component {
  constructor(props) {
    super(props);

    this.state = {
      productData: [],
      failedAPI: false,
      fetchedDataForProduct: false,
    };
  }

  componentDidMount() {
    this.fetchDataForProduct();
  }

  fetchDataForProduct = () => {
    axios.get("https://fakestoreapi.com/products")
      .then((response) => {
        this.setState({
          productData: response.data,
          fetchedDataForProduct: true,
        });
      })
      .catch((err) => {
        this.setState = {
          fetchDataForProduct: true,
          failedAPI: true,
        };
      });
  };


  render() {
    return this.state.fetchedDataForProduct ? (
      <div>
        {this.props.product.map((item) => {
          return (
            <div key={item.productId}>
              {this.state.productData.map((productList) => {
                if (productList.id === item.productId) {
                  return (
                    <div key={productList.id}>
                      <div className="col-sm-6 m-auto">
                        <div className="card rounded border-primary m-2">
                          <div className="card-body text-center ">
                            <img style={{width:"200px",height:"200px"}} src={productList.image} alt="" />
                            <h5 className="card-title">{productList.title}</h5>
                            <p className="card-text">Price: ${productList.price
                            } for 1N</p>
                            <h2 className="card-text">${productList.price * item.quantity} </h2>
                            <h4 className="card-text">
                              quantity: {item.quantity}
                            </h4>
                          </div>
                        </div>
                      </div>
                    </div>
                  );
                }
               })
               }
            </div>
          );
        })}
      </div>
    ) : this.state.failedAPI ? (
      "Sorry data not fetch"
    ) : (
      <Loader className="loader" />
    );
  }
}
