import React, { Component } from "react";
import "bootstrap/dist/css/bootstrap.min.css";

export default class Detail extends Component {
  constructor(props) {
    super(props);
    this.state = {

    };
  }
  

  render() {
    return (
      <div className="col ">
        <div className="card m-3 border border-primary">
          <img className="card-img-top " src={this.props.imgLink} alt="" />

          <div className="card-body d-flex flex-column justify-content-between ">
            <p className="card-title w-100 text-center">{this.props.title}</p>
            <p className="card-text text-center">${this.props.price}</p>
            <p className="card-text text-center">Rating: {this.props.rate}</p>
            <p className="card-text text-center">Count: {this.props.count}</p>
            <button className="btn btn-primary">Buy Now</button>
          </div>

        </div>
      </div>
    );
  }
}
