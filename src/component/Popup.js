import React, { Component } from "react";
import axios from "axios";
import Loader from "./Loader";
import { Link } from "react-router-dom";

export default class Popup extends Component {
  constructor(props) {
    super(props);
    this.state = {
      products: {},
      failedAPI: false,
      fetchedData: false,
    };
  }

  fetchData = () => {
    if (
      this.props.match.params.id < 1 ||
      this.props.match.params.id > 20 ||
      isNaN(this.props.match.params.id)
    ) {
      this.setState({
        failedAPI: true,
      });
    } else {
      axios.get(`https://fakestoreapi.com/products/${this.props.match.params.id}`)
        .then((response) => {
          return response.data;
        })
        .then((data) => {
          this.setState({
            products: data,
            fetchedData: true,
          });
        })
        .catch((err) => {
          this.setState = {
            fetchedData: true,
            failedAPI: true,
          };
        });
    }
  };

  componentDidMount() {
    this.fetchData();
  }

  render() {
    let info = this.state.products;

    return this.state.fetchedData ? (
      <div>
        <header>
          <nav className="navbar navbar-light bg-primary mb-4  d-flex flex-row justify-content-evenly">
            <h1 className="navbar-brand text-light">Single Product Detail</h1>
            <Link to={`/`}>
              <button className="btn btn-outline-light text-light ">
                Home
              </button>
            </Link>
          </nav>
        </header>

        <div className="col ">
          <div className="card h-100  w-50 border border-primary m-auto">
            <img
              className="card-img-top p-2"
              src={this.state.products.image}
              alt=""
            />
            <p className="card-title w-100 text-center text-primary text-capitalize  font-weight-bold">
              {info.category}
            </p>

            <div className="card-body d-flex flex-column justify-content-between ">
              <p className="card-text text-center">${info.price}</p>

              <p className="card-text text-center">
                Description: {info.description}
              </p>
              
              <button className="btn btn-primary">Buy Now</button>
            </div>
          </div>
        </div>
      </div>

    ) : this.state.failedAPI ? (
      <div className="text-center m-5 text-primary">
        Oops! Sorry data not fetch
      </div>
    ) : (
      <Loader className="loader" />
    );
  }
}
